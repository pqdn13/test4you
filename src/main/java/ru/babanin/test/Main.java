package ru.babanin.test;

import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.GenericDao;
import ru.babanin.test.db.dao.Identified;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.*;
import ru.babanin.test.db.sql.SqlDaoFactory;
import ru.babanin.test.xml.container.*;
import ru.babanin.test.xml.XmlSaver;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {
    private static final DaoFactory<Connection> factory = new SqlDaoFactory();
    private static Connection connection;

    public static void main(String[] args) throws PersistException, SQLException, JAXBException {
        connection = factory.getContext();

//        XmlSaver.backUp("users", new XmlUserContainer(factory.getDao(connection, User.class).getAll()));
//        XmlSaver.backUp("tests", new XmlTestContainer(factory.getDao(connection, Test.class).getAll()));
//        XmlSaver.backUp("admin2test", new XmlAdmin2testContainer(factory.getDao(connection, Admin2test.class).getAll()));
//        XmlSaver.backUp("session", new XmlSessionContainer(factory.getDao(connection, Session.class).getAll()));
//        XmlSaver.backUp("status2test", new XmlStatus2testContainer(factory.getDao(connection, Status2test.class).getAll()));
//        XmlSaver.backUp("question", new XmlQuestionContainer(factory.getDao(connection, Question.class).getAll()));
//        XmlSaver.backUp("pass_test", new XmlPassTestContainer(factory.getDao(connection, PassTest.class).getAll()));

        XmlContainer userContainer = XmlSaver.restore("users", XmlUserContainer.class);
        XmlContainer testContainer = XmlSaver.restore("tests", XmlTestContainer.class);
        XmlContainer admin2testContainer = XmlSaver.restore("admin2test", XmlAdmin2testContainer.class);
        XmlContainer sessionContainer = XmlSaver.restore("session", XmlSessionContainer.class);
        XmlContainer status2testContainer = XmlSaver.restore("status2test", XmlStatus2testContainer.class);
        XmlContainer questionContainer = XmlSaver.restore("question", XmlQuestionContainer.class);
        XmlContainer passTestContainer = XmlSaver.restore("pass_test", XmlPassTestContainer.class);

        printContainer("users:", userContainer);
        printContainer("tests:", testContainer);
        printContainer("admin2test:", admin2testContainer);
        printContainer("session:", sessionContainer);
        printContainer("status2test:", status2testContainer);
        printContainer("question:", questionContainer);
        printContainer("pass_test:", passTestContainer);

        connection.close();
    }

    public static <T> void printContainer(String s, XmlContainer<T> container){
        System.out.println("\n" + s);
        for (T t : container.getList()) {
            System.out.println(t);
        }
    }

    public static void clearTable(String s, GenericDao dao) throws PersistException {
        List<Identified<Long>> list = dao.getAll();

        System.out.println("\n" + s);
        for (Identified<Long> o : list) {
            dao.delete(o);
        }
    }

}
