package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.Session;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSessionContainer implements XmlContainer<Session> {
    @XmlElement(name = "session")
    public List<Session> list;

    public XmlSessionContainer() {
        list = null;
    }

    public XmlSessionContainer(List<Session> list) {
        this.list = list;
    }

    @Override
    public List<Session> getList() {
        return list;
    }

    @Override
    public void setList(List<Session> list) {
        this.list = list;
    }
}
