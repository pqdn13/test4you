package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.PassTest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlPassTestContainer implements XmlContainer<PassTest> {
    @XmlElement(name = "passTest")
    public List<PassTest> list;

    public XmlPassTestContainer() {
        list = null;
    }

    public XmlPassTestContainer(List<PassTest> list) {
        this.list = list;
    }

    @Override
    public List<PassTest> getList() {
        return list;
    }

    @Override
    public void setList(List<PassTest> list) {
        this.list = list;
    }
}
