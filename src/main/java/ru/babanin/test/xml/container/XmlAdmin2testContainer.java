package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.Admin2test;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlAdmin2testContainer implements XmlContainer<Admin2test>{
    @XmlElement(name = "admin2test")
    public List<Admin2test> list;

    public XmlAdmin2testContainer() {
        list = null;
    }

    public XmlAdmin2testContainer(List<Admin2test> list) {
        this.list = list;
    }

    @Override
    public List<Admin2test> getList() {
        return list;
    }

    @Override
    public void setList(List<Admin2test> list) {
        this.list = list;
    }
}
