package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.Test;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlTestContainer implements XmlContainer<Test> {
    @XmlElement(name = "test")
    public List<Test> list;

    public XmlTestContainer() {
        list = null;
    }

    public XmlTestContainer(List<Test> list) {
        this.list = list;
    }

    @Override
    public List<Test> getList() {
        return list;
    }

    @Override
    public void setList(List<Test> list) {
        this.list = list;
    }
}
