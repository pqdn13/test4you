package ru.babanin.test.xml.container;

import java.util.List;

public interface XmlContainer<T> {
    List<T> getList();
    void setList(List<T> list);
}