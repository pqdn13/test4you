package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.User;

import javax.xml.bind.annotation.*;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlUserContainer implements XmlContainer<User>{
    @XmlElement(name = "user")
    public List<User> list;

    public XmlUserContainer() {
        list = null;
    }

    public XmlUserContainer(List<User> list) {
        this.list = list;
    }


    @Override
    public List<User> getList() {
        return list;
    }

    @Override
    public void setList(List<User> list) {
        this.list = list;
    }
}
