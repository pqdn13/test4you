package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.Question;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlQuestionContainer implements XmlContainer<Question> {
    @XmlElement(name = "question")
    public List<Question> list;

    public XmlQuestionContainer() {
        list = null;
    }

    public XmlQuestionContainer(List<Question> list) {
        this.list = list;
    }

    @Override
    public List<Question> getList() {
        return list;
    }

    @Override
    public void setList(List<Question> list) {
        this.list = list;
    }
}
