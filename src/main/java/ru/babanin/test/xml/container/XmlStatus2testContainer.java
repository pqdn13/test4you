package ru.babanin.test.xml.container;

import ru.babanin.test.db.domain.Status2test;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "container")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlStatus2testContainer implements XmlContainer<Status2test> {
    @XmlElement(name = "status2test")
    public List<Status2test> list;

    public XmlStatus2testContainer() {
        this.list = null;
    }

    public XmlStatus2testContainer(List<Status2test> list) {
        this.list = list;
    }

    @Override
    public List<Status2test> getList() {
        return list;
    }

    @Override
    public void setList(List<Status2test> list) {
        this.list = list;
    }
}
