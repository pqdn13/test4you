package ru.babanin.test.xml;

import ru.babanin.test.db.dao.GenericDao;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.xml.container.XmlContainer;
import ru.babanin.test.xml.container.XmlUserContainer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

/**
 * Created by makcim on 23.02.17.
 */
public class XmlSaver {
    private static final String pathToBackUpFolder = "backup" + File.separator;

    public static void convertObjectToXml(Object obj, String filePath) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(obj, new File(filePath));
    }


    public static Object fromXmlToObject(Class type, String filePath) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(type);
        Unmarshaller un = jaxbContext.createUnmarshaller();
        return un.unmarshal(new File(filePath));
    }

    public static void backUp(String s, XmlContainer<?> container) throws PersistException, JAXBException {
        String fileName = pathToBackUpFolder + s + ".xml";
        XmlSaver.convertObjectToXml(container, fileName);
    }

    public static XmlContainer restore(String s, Class<? extends XmlContainer> type) throws PersistException, JAXBException {
        String fileName = pathToBackUpFolder + s + ".xml";
        return (XmlContainer)fromXmlToObject(type, fileName);
    }
}
