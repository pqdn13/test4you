package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.Status2test;
import ru.babanin.test.db.domain.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SqlStatus2test extends AbstractJDBCDao<Status2test, Long> {
    private class Persist extends Status2test {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlStatus2test(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
        addRelation(Status2test.class, "test");
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM status2test";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO status2test (id_test, name, min_mark, max_mark) \n" +
                "VALUES (?, ?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM status2test WHERE id= ?;";
    }

    @Override
    public Status2test create() throws PersistException {
        return persist(new Status2test());
    }

    @Override
    protected List<Status2test> parseResultSet(ResultSet rs) throws PersistException {
        List<Status2test> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Persist persist = new Persist();
                persist.setId(rs.getLong("id"));
                persist.setTest((Test) getDependence(Test.class, rs.getLong("id_test")));
                persist.setName(rs.getString("name"));
                persist.setMin_mark(rs.getLong("min_mark"));
                persist.setMax_mark(rs.getLong("max_mark"));
                result.add(persist);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Status2test object) throws PersistException {
        prepareStatementForInsert(statement, object);
        try {
            statement.setLong(5, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Status2test object) throws PersistException {
        try {
            long id_test = (object.getTest() == null || object.getTest().getId() == null) ? -1
                    : object.getTest().getId();

            statement.setLong(1, id_test);
            statement.setString(2, object.getName());
            statement.setLong(3, object.getMin_mark());
            statement.setLong(4, object.getMax_mark());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
