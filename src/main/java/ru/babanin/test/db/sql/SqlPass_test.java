package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.PassTest;
import ru.babanin.test.db.domain.Question;
import ru.babanin.test.db.domain.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by makcim on 23.02.17.
 */
public class SqlPass_test extends AbstractJDBCDao<PassTest, Long> {
    private class Persist extends PassTest {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlPass_test(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
        addRelation(PassTest.class, "session");
        addRelation(PassTest.class, "question");
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM pass_test";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO pass_test (id_session, id_quest, mark) \n" +
                "VALUES (?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM pass_test WHERE id= ?;";
    }

    @Override
    public PassTest create() throws PersistException {
        return persist(new PassTest());
    }

    @Override
    protected List<PassTest> parseResultSet(ResultSet rs) throws PersistException {
        List<PassTest> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Persist persist = new Persist();
                persist.setId(rs.getLong("id"));
                persist.setSession((Session) getDependence(Session.class, rs.getLong("id_session")));
                persist.setQuestion((Question) getDependence(Question.class, rs.getLong("id_quest")));
                persist.setMark(rs.getLong("mark"));
                result.add(persist);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, PassTest object) throws PersistException {
        prepareStatementForInsert(statement, object);
        try {
            statement.setLong(4, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, PassTest object) throws PersistException {
        try {
            long id_session = (object.getSession() == null || object.getSession().getId() == null) ? -1
                    : object.getSession().getId();

            long id_quest = (object.getQuestion() == null || object.getQuestion().getId() == null) ? -1
                    : object.getQuestion().getId();

            statement.setLong(1, id_session);
            statement.setLong(2, id_quest);
            statement.setLong(3, object.getMark());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
