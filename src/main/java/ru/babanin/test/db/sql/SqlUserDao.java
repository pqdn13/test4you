package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SqlUserDao  extends AbstractJDBCDao<User, Long> {
    private class Persist extends User {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlUserDao(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM users";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO users (login, hash_password, access_level, name, surname, email, check_in, money) \n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM users WHERE id= ?;";
    }

    @Override
    public User create() throws PersistException {
        return persist(new User());
    }

    @Override
    protected List<User> parseResultSet(ResultSet rs) throws PersistException {
        List<User> result = new ArrayList<>();
        try {
            while (rs.next()) {
//                User user = new User();
                Persist user = new Persist();
                user.setId(rs.getLong("id"));
                user.setLogin(rs.getString("login"));
                user.setHash_password(rs.getLong("hash_password"));
                user.setAccess_level(rs.getLong("access_level"));
                user.setName(rs.getString("name"));
                user.setSurname(rs.getString("surname"));
                user.setEmail(rs.getString("email"));
                user.setCheck_in(rs.getTimestamp("check_in"));
                user.setMoney(rs.getLong("money"));
                result.add(user);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, User object) throws PersistException {
        try {
            statement.setString(1, object.getLogin());
            statement.setLong(2, object.getHash_password());
            statement.setLong(3, object.getAccess_level());
            statement.setString(4, object.getName());
            statement.setString(5, object.getSurname());
            statement.setString(6, object.getEmail());
            statement.setTimestamp(7, new Timestamp(object.getCheck_in().getTime()));
            statement.setLong(7, object.getMoney());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, User object) throws PersistException {
        try {
            statement.setString(1, object.getLogin());
            statement.setLong(2, object.getHash_password());
            statement.setLong(3, object.getAccess_level());
            statement.setString(4, object.getName());
            statement.setString(5, object.getSurname());
            statement.setString(6, object.getEmail());
            statement.setTimestamp(7, new Timestamp(object.getCheck_in().getTime()));
            statement.setLong(7, object.getMoney());
            statement.setLong(8, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
