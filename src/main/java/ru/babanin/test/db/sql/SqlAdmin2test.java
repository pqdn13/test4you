package ru.babanin.test.db.sql;


import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.Admin2test;
import ru.babanin.test.db.domain.Test;
import ru.babanin.test.db.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SqlAdmin2test  extends AbstractJDBCDao<Admin2test, Long> {
    private class Persist extends Admin2test {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlAdmin2test(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
        addRelation(Admin2test.class, "user");
        addRelation(Admin2test.class, "test");
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM admin2test";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO admin2test (id_test, id_user) \n" +
                "VALUES (?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM admin2test WHERE id= ?;";
    }

    @Override
    public Admin2test create() throws PersistException {
        return persist(new Admin2test());
    }

    @Override
    protected List<Admin2test> parseResultSet(ResultSet rs) throws PersistException {
        List<Admin2test> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Persist persist = new Persist();
                persist.setId(rs.getLong("id"));
                persist.setTest((Test) getDependence(Test.class, rs.getLong("id_test")));
                persist.setUser((User) getDependence(User.class, rs.getLong("id_user")));
                result.add(persist);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Admin2test object) throws PersistException {
        prepareStatementForInsert(statement, object);
        try {
            statement.setLong(3, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Admin2test object) throws PersistException {
        try {
            long id_test = (object.getTest() == null || object.getTest().getId() == null) ? -1
                    : object.getTest().getId();

            long id_user = (object.getUser() == null || object.getUser().getId() == null) ? -1
                    : object.getTest().getId();

            statement.setLong(1, id_test);
            statement.setLong(2, id_user);
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

}
