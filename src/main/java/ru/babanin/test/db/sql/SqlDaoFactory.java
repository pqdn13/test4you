package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.GenericDao;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SqlDaoFactory implements DaoFactory<Connection> {
    private final String user = "makcim";
    private final String password = "123456";
    private final String url = "jdbc:postgresql://localhost:5432/students";
    private final String driver = "org.postgresql.Driver";
    private Map<Class, DaoCreator> creators;

    public Connection getContext() throws PersistException {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return  connection;
    }

    @Override
    public GenericDao getDao(Connection connection, Class dtoClass) throws PersistException {
        DaoCreator creator = creators.get(dtoClass);
        if (creator == null) {
            throw new PersistException("Dao object for " + dtoClass + " not found.");
        }
        return creator.create(connection);
    }

    public SqlDaoFactory() {
        try {
            Class.forName(driver);//Регистрируем драйвер
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        creators = new HashMap<>();
        creators.put(User.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlUserDao(SqlDaoFactory.this, connection);
            }
        });

        creators.put(Test.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlTestDao(SqlDaoFactory.this, connection);
            }
        });

        creators.put(Admin2test.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlAdmin2test(SqlDaoFactory.this, connection);
            }
        });

        creators.put(Session.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlSession(SqlDaoFactory.this, connection);
            }
        });

        creators.put(Status2test.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlStatus2test(SqlDaoFactory.this, connection);
            }
        });

        creators.put(Question.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlQuestion(SqlDaoFactory.this, connection);
            }
        });

        creators.put(PassTest.class, new DaoCreator<Connection>() {
            @Override
            public GenericDao create(Connection connection) {
                return new SqlPass_test(SqlDaoFactory.this, connection);
            }
        });
    }
}
