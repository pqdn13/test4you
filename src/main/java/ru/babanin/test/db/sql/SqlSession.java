package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.Session;
import ru.babanin.test.db.domain.Test;
import ru.babanin.test.db.domain.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class SqlSession extends AbstractJDBCDao<Session, Long> {
    private class Persist extends Session {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlSession(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
        addRelation(Session.class, "user");
        addRelation(Session.class, "test");
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM session";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO session (id_test, id_user, time) \n" +
                "VALUES (?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM session WHERE id= ?;";
    }

    @Override
    public Session create() throws PersistException {
        return persist(new Session());
    }

    @Override
    protected List<Session> parseResultSet(ResultSet rs) throws PersistException {
        List<Session> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Persist persist = new Persist();
                persist.setId(rs.getLong("id"));
                persist.setTime(new Date(rs.getTimestamp("time").getTime()));
                persist.setTest((Test) getDependence(Test.class, rs.getLong("id_test")));
                persist.setUser((User) getDependence(User.class, rs.getLong("id_user")));
                result.add(persist);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Session object) throws PersistException {
        prepareStatementForInsert(statement, object);
        try {
            statement.setLong(4, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Session object) throws PersistException {
        try {
            long id_test = (object.getTest() == null || object.getTest().getId() == null) ? -1
                    : object.getTest().getId();

            long id_user = (object.getUser() == null || object.getUser().getId() == null) ? -1
                    : object.getUser().getId();

            statement.setLong(1, id_test);
            statement.setLong(2, id_user);
            statement.setTimestamp(3, new Timestamp(object.getTime().getTime()));
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
