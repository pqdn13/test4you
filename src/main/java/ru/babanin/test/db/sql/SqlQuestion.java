package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.Question;
import ru.babanin.test.db.domain.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class SqlQuestion extends AbstractJDBCDao<Question, Long> {
    private class Persist extends Question {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlQuestion(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
        addRelation(Question.class, "test");
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM questions";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO questions (text, criterion, max_mark, id_test) \n" +
                "VALUES (?, ?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM questions WHERE id= ?;";
    }

    @Override
    public Question create() throws PersistException {
        return persist(new Question());
    }

    @Override
    protected List<Question> parseResultSet(ResultSet rs) throws PersistException {
        List<Question> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Persist persist = new Persist();
                persist.setId(rs.getLong("id"));
                persist.setText(rs.getString("text"));
                persist.setCriterion(rs.getString("criterion"));
                persist.setMax_mark(rs.getLong("max_mark"));
                persist.setTest((Test) getDependence(Test.class, rs.getLong("id_test")));
                result.add(persist);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Question object) throws PersistException {
        prepareStatementForInsert(statement, object);
        try {
            statement.setLong(5, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Question object) throws PersistException {
        try {
            long id_test = (object.getTest() == null || object.getTest().getId() == null) ? -1
                    : object.getTest().getId();

            statement.setString(1, object.getText());
            statement.setString(2, object.getCriterion());
            statement.setLong(3, object.getMax_mark());
            statement.setLong(4, id_test);
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }
}
