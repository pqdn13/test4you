package ru.babanin.test.db.sql;

import ru.babanin.test.db.dao.AbstractJDBCDao;
import ru.babanin.test.db.dao.DaoFactory;
import ru.babanin.test.db.dao.PersistException;
import ru.babanin.test.db.domain.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SqlTestDao extends AbstractJDBCDao<Test, Long> {
    private class Persist extends Test {
        public void setId(long id) {
            super.setId(id);
        }
    }

    public SqlTestDao(DaoFactory<Connection> parentFactory, Connection connection) {
        super(parentFactory, connection);
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM tests";
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO tests (name, access_level, hash_password, max_mark, max_try, cost, birth) \n" +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";
    }

    @Override
    public String getUpdateQuery() {
        return "";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM tests WHERE id= ?;";
    }

    @Override
    public Test create() throws PersistException {
        return persist(new Test());
    }

    @Override
    protected List<Test> parseResultSet(ResultSet rs) throws PersistException {
        List<Test> result = new ArrayList<>();
        try {
            while (rs.next()) {
                Persist persist = new Persist();
                persist.setId(rs.getLong("id"));
                persist.setName(rs.getString("name"));
                persist.setAccess_level(rs.getLong("access_level"));
                persist.setHash_password(rs.getLong("hash_password"));
                persist.setMax_mark(rs.getLong("max_mark"));
                persist.setMax_try(rs.getLong("max_try"));
                persist.setCost(rs.getLong("cost"));
                persist.setBirth(new Date(rs.getTimestamp("birth").getTime()));
                result.add(persist);
            }
        } catch (Exception e) {
            throw new PersistException(e);
        }
        return result;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Test object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getAccess_level());
            statement.setLong(3, object.getHash_password());
            statement.setLong(4, object.getMax_mark());
            statement.setLong(5, object.getMax_try());
            statement.setLong(6, object.getCost());
            statement.setTimestamp(7, new Timestamp(object.getBirth().getTime()));
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }

    @Override
    protected void prepareStatementForUpdate(PreparedStatement statement, Test object) throws PersistException {
        try {
            statement.setString(1, object.getName());
            statement.setLong(2, object.getAccess_level());
            statement.setLong(3, object.getHash_password());
            statement.setLong(4, object.getMax_mark());
            statement.setLong(5, object.getMax_try());
            statement.setLong(6, object.getCost());
            statement.setTimestamp(7, new Timestamp(object.getBirth().getTime()));
            statement.setLong(8, object.getId());
        } catch (Exception e) {
            throw new PersistException(e);
        }
    }


}
