package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

@XmlRootElement(name = "Test")
public class Test implements Identified<Long> {
  private Long id;
  private String name;
  private Long access_level;
  private Long hash_password;
  private Long max_mark;
  private Long max_try;
  private Long cost;
  private Date birth;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @XmlAttribute
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getAccess_level() {
    return access_level;
  }

  public void setAccess_level(Long access_level) {
    this.access_level = access_level;
  }

  public Long getHash_password() {
    return hash_password;
  }

  public void setHash_password(Long hash_password) {
    this.hash_password = hash_password;
  }

  public Long getMax_mark() {
    return max_mark;
  }

  public void setMax_mark(Long max_mark) {
    this.max_mark = max_mark;
  }

  public Long getMax_try() {
    return max_try;
  }

  public void setMax_try(Long max_try) {
    this.max_try = max_try;
  }

  public Long getCost() {
    return cost;
  }

  public void setCost(Long cost) {
    this.cost = cost;
  }

  public Date getBirth() {
    return birth;
  }

  public void setBirth(Date birth) {
    this.birth = birth;
  }

  @Override
  public String toString() {
    return "[Test: " + getName() + ", cost " + getCost() + "]";
  }
}
