package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "PassTest")
public class PassTest implements Identified<Long> {
  private Long id;
  private Session session;
  private Question question;
  private Long mark;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Session getSession() {
    return session;
  }

  public void setSession(Session session) {
    this.session = session;
  }

  public Question getQuestion() {
    return question;
  }

  public void setQuestion(Question question) {
    this.question = question;
  }

  public Long getMark() {
    return mark;
  }

  public void setMark(Long mark) {
    this.mark = mark;
  }

  @Override
  public String toString() {
    return "{" + session +
            " " + question +
            ": " + mark +
            '}';
  }
}
