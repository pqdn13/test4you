package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "Admin2test")
public class Admin2test implements Identified<Long>{
  private Long id = null;
  private Test test;
  private User user;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Test getTest() {
    return test;
  }

  public void setTest(Test test) {
    this.test = test;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

    @Override
    public String toString() {
        return "{" + getUser() + ", " + getTest() + "}";
    }
}
