package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

@XmlRootElement(name = "Session")
public class Session implements Identified<Long> {
  private Long id = null;
  private Test test;
  private User user;
  private Date time;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Test getTest() {
    return test;
  }

  public void setTest(Test test) {
    this.test = test;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  @Override
  public String toString() {
    return "{" + getUser() + ", " + getTest() + ": " + getTime() + "}";
  }
}
