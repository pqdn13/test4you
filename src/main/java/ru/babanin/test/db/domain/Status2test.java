package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "Status2test")
public class Status2test implements Identified<Long> {
  private Long id;
  private Test test;
  private String name;
  private Long min_mark;
  private Long max_mark;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Test getTest() {
    return test;
  }

  public void setTest(Test test) {
    this.test = test;
  }

  @XmlAttribute
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getMin_mark() {
    return min_mark;
  }

  public void setMin_mark(Long min_mark) {
    this.min_mark = min_mark;
  }

  public Long getMax_mark() {
    return max_mark;
  }

  public void setMax_mark(Long max_mark) {
    this.max_mark = max_mark;
  }

  @Override
  public String toString() {
    return "{" + test +
            ": '" + name + ' ' +
            "from " + min_mark +
            " to " + max_mark +
            '}';
  }
}
