package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Date;

@XmlRootElement(name = "User")
public class User implements Identified<Long> {
  private Long id = null;
  private String login;
  private Long hash_password;
  private Long access_level;
  private String name;
  private String surname;
  private String email;
  private Date check_in;
  private Long money;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @XmlAttribute
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public Long getHash_password() {
    return hash_password;
  }

  public void setHash_password(Long hash_password) {
    this.hash_password = hash_password;
  }

  public Long getAccess_level() {
    return access_level;
  }

  public void setAccess_level(Long access_level) {
    this.access_level = access_level;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @XmlAttribute
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Date getCheck_in() {
    return check_in;
  }

  public void setCheck_in(Date check_in) {
    this.check_in = check_in;
  }

  public Long getMoney() {
    return money;
  }

  public void setMoney(Long money) {
    this.money = money;
  }

  @Override
  public String toString() {
    return "[" + getLogin() + ": " + getName() + " " + getSurname() + "]";
  }
}
