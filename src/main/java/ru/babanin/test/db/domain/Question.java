package ru.babanin.test.db.domain;

import ru.babanin.test.db.dao.Identified;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name = "Question")
public class Question implements Identified<Long> {
  private Long id;
  private String text;
  private String criterion;
  private Long max_mark;
  private Test test;

  @XmlTransient
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public String getCriterion() {
    return criterion;
  }

  public void setCriterion(String criterion) {
    this.criterion = criterion;
  }

  public Long getMax_mark() {
    return max_mark;
  }

  public void setMax_mark(Long max_mark) {
    this.max_mark = max_mark;
  }

  public Test getTest() {
    return test;
  }

  public void setTest(Test test) {
    this.test = test;
  }

    @Override
    public String toString() {
        return "{" + test +
                ": " + text + "? " + max_mark +
                '}';
    }
}
