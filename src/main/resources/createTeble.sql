DROP TABLE users CASCADE;
DROP TABLE tests CASCADE;
DROP TABLE questions CASCADE;
DROP TABLE admin2test CASCADE;
DROP TABLE status2test CASCADE;
DROP TABLE pass_test CASCADE;
DROP TABLE dump CASCADE;

CREATE TABLE people (
  ID            SERIAL PRIMARY KEY,
  login         VARCHAR(60) NOT NULL,
  hash_password BIGINT,
  access_level  SMALLINT NOT NULL,
  name    VARCHAR(16) NOT NULL,
  surname   VARCHAR(16) NOT NULL,
  email         VARCHAR(60) NOT NULL,
  birth         DATE,
  check_in      DATE NOT NULL,
  last_edit     DATE NOT NULL,
  money         BIGINT
);



CREATE TABLE tests (
  ID            SERIAL PRIMARY KEY,
  name          VARCHAR(120) NOT NULL,
  access_level  SMALLINT NOT NULL,
  hash_password BIGINT,
  max_mark      INT NOT NULL,
  max_try       SMALLINT,
  cost          BIGINT,
  birth         DATE NOT NULL,
  last_edit     DATE NOT NULL
);

CREATE TABLE admin2test (
  ID      SERIAL PRIMARY KEY,
  id_test BIGINT NOT NULL,
  id_user BIGINT NOT NULL,
  FOREIGN KEY (id_test) REFERENCES tests (ID),
  FOREIGN KEY (id_user) REFERENCES users (ID)
);


CREATE TABLE questions (
  ID         SERIAL PRIMARY KEY,
  text       TEXT NOT NULL,
  criterion TEXT,
  max_mark   INT NOT NULL,
  id_test    BIGINT NOT NULL,
  FOREIGN KEY (id_test) REFERENCES tests (ID)
);

CREATE TABLE status2test (
  ID       SERIAL PRIMARY KEY,
  id_test  BIGINT NOT NULL,
  name VARCHAR(16) NOT NULL,
  min_mark INT NOT NULL,
  max_mark INT NOT NULL,
  FOREIGN KEY (id_test) REFERENCES tests (ID)
);


CREATE TABLE session (
  ID      SERIAL PRIMARY KEY,
  id_user BIGINT NOT NULL,
  id_test BIGINT NOT NULL,
  time    TIMESTAMP NOT NULL ,
  FOREIGN KEY (id_test) REFERENCES tests (ID),
  FOREIGN KEY (id_user) REFERENCES users (ID)
);


CREATE TABLE pass_test (
  ID         SERIAL PRIMARY KEY,
  id_session BIGINT NOT NULL,
  id_quest   BIGINT NOT NULL,
  mark       INT,
  FOREIGN KEY (id_session) REFERENCES session (ID),
  FOREIGN KEY (id_quest) REFERENCES questions (ID)
);










