INSERT INTO
  users (login, hash_password, access_level,
          name, surname, email,
          birth, check_in, last_edit, money)
VALUES (3, 3, '2017-02-17');



INSERT INTO student (name, age) VALUES ('Вася', 25);
INSERT INTO lesson (name) VALUES ('Java Core');
INSERT INTO journal (id_student, id_lesson, date) VALUES (3, 3, '2017-02-17');
INSERT INTO journal (id_student, id_lesson, date) VALUES (3, 3, '18.02.2017');

select * from student;
select * from lesson;
select * from journal;

delete from student;
delete from lesson;

SELECT l.name AS lesson, s.name AS student, j.date as mydate
FROM journal j
  JOIN student as s ON (j.id_student = s.id)
  JOIN lesson as l ON (j.id_lesson = l.id);